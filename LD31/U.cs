﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public static class U
{
    public static TValue Get<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue @default = default(TValue))
    {
        TValue v;
        if (dict.TryGetValue(key, out v))
            return v;
        return @default;
    }
}