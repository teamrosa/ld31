﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LD31
{
    public class World
    {
        public readonly int Size = 7;
        public readonly int Scale = 3;
        public readonly int CellSpacing = 15;
        public readonly int TileSize = 25;
        public readonly IGameObject[,] Stuff;

        public IGameObject this[Vec2i p]
        {
            get { return this[p.y, p.x]; }
            set { this[p.y, p.x] = value; }
        }


        public IGameObject this[int r, int c]
        {
            get { return Stuff[r, c]; }
            set { Stuff[r, c] = value; }
        }

        public World()
        {
            Stuff = new IGameObject[Size, Size];
        }

        public bool InBounds(Vec2i pos)
        {
            return pos.x >= 0 && pos.y >= 0 && pos.y < Size && pos.x < Size;
        }
        static readonly Texture2D grass = new Texture2D("res/ground.png");

        public void DrawCell(GL gl, Texture2D texture, Vec2i pos, bool flip = false, Vec2i offset = null)
        {
            //Console.WriteLine(pos.ToString());
            gl.ctx.Save();
            gl.ctx.Translate(0, gl.ctx.Canvas.Height);
            gl.ctx.Scale(Scale, Scale);
            
            gl.ctx.Translate(CellSpacing * pos.x , -texture.img.Height - CellSpacing * pos.y);

            if (offset != null)
                gl.ctx.Translate(offset.x, -offset.y);

            if (flip)
            {
                gl.ctx.Scale(-1, 1);
                gl.ctx.Translate(texture.img.Width/2 - CellSpacing, 0);
            }

            gl.DrawTexture(texture, 0, 0);

            gl.ctx.Restore();
        }

        public void Draw(GL gl)
        {
            //Draw grass
            for (int row = 0; row < Size; row++)
                for (int col = 0; col < Size; col++)
                    DrawCell(gl, grass, new Vec2i(col, row));

            for (int y = Size - 1; y >= 0; y--)
            {
                for (int x = 0; x < Size; x++)
                {
                    var t = this[y, x];
                    if (t != null)
                        t.Draw(gl);
                }
            }
        }

        public void Update()
        {
            List<IGameObject> toUpdate = new List<IGameObject>();
            foreach (var thing in Stuff)
                if (thing != null)
                    toUpdate.Add(thing);
            foreach (var thing in toUpdate)
                thing.Update();

        }
    }
}