﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LD31
{
    public class Entity : GameObject
    {
        //Can hold
        //
        public Entity(World world, Vec2i pos) : base(world, pos) { }

        public Texture2D texture_jump;
        public Texture2D texture_idle;


        public override Texture2D Texture
        {
            get
            {
                if (AnimationFramesLeft > 0)
                    return texture_jump;
                return texture_idle;
            }
        }

        public const int MoveNumFrames = 15;
        protected int AnimationFramesLeft = 0;
        public int JumpHeight = 5;

        Vec2i lastMove = Vec2i.Zero;

        public override Vec2i Offset
        {
            get
            {
                //Whatever. Going to just assume the are both 15.
                //return null;
                if (AnimationFramesLeft > 0)
                    return -AnimationFramesLeft * lastMove + (int)(JumpHeight - JumpHeight * Math.Pow(2 * AnimationFramesLeft / (double)MoveNumFrames - 1, 2)) * Vec2i.Up;
                return null;
            }
        }
        public override bool Fliped
        {
            get
            {
                return left;
            }
        }

        protected bool left = false;

        public virtual Vec2i GetMove()
        {
            var move = Vec2i.Zero;
            if (Keyboard.GetPressed(Key.W) | Keyboard.GetDown(Key.Up))
                move += Vec2i.Up;
            if (Keyboard.GetPressed(Key.S) | Keyboard.GetDown(Key.Down))
                move += Vec2i.Down;
            if (Keyboard.GetPressed(Key.A) | Keyboard.GetDown(Key.Left))
                move += Vec2i.Left;

            if (Keyboard.GetPressed(Key.D) | Keyboard.GetDown(Key.Right))
                move += Vec2i.Right;

            return move;
        }

        public override void Update()
        {
            if (AnimationFramesLeft > 0)
            {
                AnimationFramesLeft--;
                return;
            }

            var move = GetMove();

            if (move == Vec2i.Zero)
                return;


            if (move.x > 0)
                left = false;
            if (move.x < 0)
                left = true;


            var newPos = P + move;
            if (world.InBounds(newPos) && world[newPos] == null)
            {
                lastMove = move;
                AnimationFramesLeft = MoveNumFrames;
                P = newPos;
            }

        }
    }
}