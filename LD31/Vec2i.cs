﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LD31
{
    public class Vec2i : IEquatable<Vec2i>
    {
        public readonly int x;
        public readonly int y;

        public Vec2i(int x = 0, int y = 0)
        {
            this.x = x; this.y = y;
        }

        public override string ToString()
        {
            return "{" + x + "," + y + "}";
        }

        public override bool Equals(Object o)
        {
            return this.Equals(o as Vec2i);
        }

        public bool Equals(Vec2i o)
        {
            if (o == null)
                return false;
            return x == o.x && y == o.y;
        }

        public override int GetHashCode()
        {
            return (x.GetHashCode() + y.GetHashCode());
        }

        public static bool operator !=(Vec2i a, Vec2i b)
        {
            if (object.Equals(null, a))
                return !object.Equals(null, b);
            return !a.Equals(b);
        }

        public static bool operator ==(Vec2i a, Vec2i b)
        {
            if (object.Equals(null, a))
                return object.Equals(null, b);
            return a.Equals(b);
        }

        public static Vec2i operator -(Vec2i a, Vec2i b)
        {
            return new Vec2i(a.x - b.x, a.y - b.y);
        }

        public static Vec2i operator *(int s, Vec2i v)
        {
            return new Vec2i(s * v.x, s * v.y);
        }

        public static Vec2i operator +(Vec2i a, Vec2i b)
        {
            return new Vec2i(a.x + b.x, a.y + b.y);
        }

        public static readonly Vec2i Zero = new Vec2i(0, 0);
        public static readonly Vec2i Right = new Vec2i(1, 0);
        public static readonly Vec2i Up = new Vec2i(0, 1);
        public static readonly Vec2i Left = new Vec2i(-1, 0);
        public static readonly Vec2i Down = new Vec2i(0, -1);
    }
}