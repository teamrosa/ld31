﻿using System;
using System.Collections.Generic;
using System.Web;

namespace LD31
{
    public class Tree : GameObject
    {
        public Tree(World world, Vec2i pos) : base(world, pos) { }

        enum Status
        {
            Sapling,
            Regular,
            Apple
        }

        public static readonly Texture2D TreeTexture = new Texture2D("res/tree.png");
        public static readonly Texture2D TreeWithAppleTexture = new Texture2D("res/tree_with_apple.png");
        public static readonly Texture2D TreeSaplingTexture = new Texture2D("res/tree_sapling.png");

        public bool HasApple
        {
            get
            {
                return Age >= TimeUntilApple;
            }
        }

        public override Texture2D Texture
        {
            get
            {
                if (IsSapling)
                    return TreeSaplingTexture;
                if (HasApple)
                    return TreeWithAppleTexture;
                return TreeTexture;
            }
        }

        public override void Update()
        {
            Age++;
        }

        public bool IsSapling
        {
            get
            {
                return Age < 0;
            }
        }

        public const int TimeAsSapling = 60 * Game.TicksPerSecond;
        public const int TimeUntilApple = 10 * Game.TicksPerSecond;

        public int Age;
    }
}