﻿(function() {
	'use strict';
	global.LD31 = global.LD31 || {};
	////////////////////////////////////////////////////////////////////////////////
	// U
	var $U = function() {
	};
	$U.__typeName = 'U';
	$U.get = function(TKey, TValue) {
		return function(dict, key, default1) {
			var v = {};
			if (dict.tryGetValue(key, v)) {
				return v.$;
			}
			return default1;
		};
	};
	global.U = $U;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Tree.Status
	var $LD31_$Tree$Status = function() {
	};
	$LD31_$Tree$Status.__typeName = 'LD31.$Tree$Status';
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Bunny
	var $LD31_Bunny = function(world, pos) {
		this.$lastMove$1 = $LD31_Vec2i.zero;
		$LD31_Entity.call(this, world, pos);
	};
	$LD31_Bunny.__typeName = 'LD31.Bunny';
	global.LD31.Bunny = $LD31_Bunny;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Entity
	var $LD31_Entity = function(world, pos) {
		this.texture_jump = null;
		this.texture_idle = null;
		this.animationFramesLeft = 0;
		this.jumpHeight = 5;
		this.$lastMove = $LD31_Vec2i.zero;
		this.left = false;
		$LD31_GameObject.call(this, world, pos);
	};
	$LD31_Entity.__typeName = 'LD31.Entity';
	global.LD31.Entity = $LD31_Entity;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Game
	var $LD31_Game = function() {
		this.gl = null;
		this.world = new $LD31_World();
	};
	$LD31_Game.__typeName = 'LD31.Game';
	global.LD31.Game = $LD31_Game;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.GameObject
	var $LD31_GameObject = function(world, pos) {
		this.world = null;
		this.$_pos = null;
		this.world = world;
		this.set_p(pos);
	};
	$LD31_GameObject.__typeName = 'LD31.GameObject';
	global.LD31.GameObject = $LD31_GameObject;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.GL
	var $LD31_GL = function(size) {
		this.$canvas = null;
		this.ctx = null;
		var $t1 = document.createElement('canvas');
		this.$canvas = ss.cast($t1, ss.isValue($t1) && (ss.isInstanceOfType($t1, Element) && $t1.tagName === 'CANVAS'));
		this.$canvas.height = this.$canvas.width = size;
		this.ctx = ss.cast(this.$canvas.getContext('2d'), CanvasRenderingContext2D);
		this.ctx.imageSmoothingEnabled = false;
		document.body.appendChild(this.$canvas);
	};
	$LD31_GL.__typeName = 'LD31.GL';
	global.LD31.GL = $LD31_GL;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.IGameObject
	var $LD31_IGameObject = function() {
	};
	$LD31_IGameObject.__typeName = 'LD31.IGameObject';
	global.LD31.IGameObject = $LD31_IGameObject;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.ImgLoader
	var $LD31_ImgLoader = function() {
	};
	$LD31_ImgLoader.__typeName = 'LD31.ImgLoader';
	$LD31_ImgLoader.get = function(path) {
		var img = {};
		if ($LD31_ImgLoader.$cashe.tryGetValue(path, img)) {
			return img.$;
		}
		var $t1 = $LD31_ImgLoader.$cashe;
		var $t2 = $LD31_ImgLoader.$load(path);
		$t1.set_item(path, $t2);
		return $t2;
	};
	$LD31_ImgLoader.$load = function(path) {
		var $t1 = new Image();
		$t1.src = path;
		return $t1;
	};
	global.LD31.ImgLoader = $LD31_ImgLoader;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Key
	var $LD31_Key = function() {
	};
	$LD31_Key.__typeName = 'LD31.Key';
	global.LD31.Key = $LD31_Key;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Keyboard
	var $LD31_Keyboard = function() {
	};
	$LD31_Keyboard.__typeName = 'LD31.Keyboard';
	$LD31_Keyboard.update = function() {
		ss.clear($LD31_Keyboard.$justPressed);
		$LD31_Keyboard.$justChanged.clear();
	};
	$LD31_Keyboard.getKey = function(key) {
		return $U.get($LD31_Key, Boolean).call(null, $LD31_Keyboard.$keysDown, key, false);
	};
	$LD31_Keyboard.getPressed = function(key) {
		return ss.contains($LD31_Keyboard.$justPressed, key);
	};
	$LD31_Keyboard.getDown = function(key) {
		return $LD31_Keyboard.getKey(key) && $U.get($LD31_Key, Boolean).call(null, $LD31_Keyboard.$justChanged, key, false);
	};
	$LD31_Keyboard.start = function() {
		document.onkeypress = function(e) {
			var k = ss.cast(e, KeyboardEvent);
			var key = k.keyCode;
			//Console.WriteLine("Key: "+key);
			ss.add($LD31_Keyboard.$justPressed, key);
		};
		document.onkeydown = function(e1) {
			var k1 = ss.cast(e1, KeyboardEvent);
			var key1 = k1.keyCode;
			$LD31_Keyboard.$justChanged.set_item(key1, !$LD31_Keyboard.getKey(key1));
			$LD31_Keyboard.$keysDown.set_item(key1, true);
		};
		document.onkeyup = function(e2) {
			var k2 = ss.cast(e2, KeyboardEvent);
			var key2 = k2.keyCode;
			$LD31_Keyboard.$justChanged.set_item(key2, $LD31_Keyboard.getKey(key2));
			$LD31_Keyboard.$keysDown.set_item(key2, false);
		};
	};
	global.LD31.Keyboard = $LD31_Keyboard;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Program
	var $LD31_Program = function() {
	};
	$LD31_Program.__typeName = 'LD31.Program';
	$LD31_Program.main = function() {
		($LD31_Program.game = new $LD31_Game()).$start();
	};
	global.LD31.Program = $LD31_Program;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Sprite
	var $LD31_Sprite = function() {
	};
	$LD31_Sprite.__typeName = 'LD31.Sprite';
	global.LD31.Sprite = $LD31_Sprite;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Texture2D
	var $LD31_Texture2D = function(name) {
		this.img = null;
		this.name = null;
		this.name = name;
		this.img = $LD31_ImgLoader.get(name);
	};
	$LD31_Texture2D.__typeName = 'LD31.Texture2D';
	global.LD31.Texture2D = $LD31_Texture2D;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Tree
	var $LD31_Tree = function(world, pos) {
		this.age = 0;
		$LD31_GameObject.call(this, world, pos);
	};
	$LD31_Tree.__typeName = 'LD31.Tree';
	global.LD31.Tree = $LD31_Tree;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.Vec2i
	var $LD31_Vec2i = function(x, y) {
		this.x = 0;
		this.y = 0;
		this.x = x;
		this.y = y;
	};
	$LD31_Vec2i.__typeName = 'LD31.Vec2i';
	$LD31_Vec2i.op_Inequality = function(a, b) {
		if (ss.staticEquals(null, a)) {
			return !ss.staticEquals(null, b);
		}
		return !a.equalsT(b);
	};
	$LD31_Vec2i.op_Equality = function(a, b) {
		if (ss.staticEquals(null, a)) {
			return ss.staticEquals(null, b);
		}
		return a.equalsT(b);
	};
	$LD31_Vec2i.op_Subtraction = function(a, b) {
		return new $LD31_Vec2i(a.x - b.x, a.y - b.y);
	};
	$LD31_Vec2i.op_Multiply = function(s, v) {
		return new $LD31_Vec2i(s * v.x, s * v.y);
	};
	$LD31_Vec2i.op_Addition = function(a, b) {
		return new $LD31_Vec2i(a.x + b.x, a.y + b.y);
	};
	global.LD31.Vec2i = $LD31_Vec2i;
	////////////////////////////////////////////////////////////////////////////////
	// LD31.World
	var $LD31_World = function() {
		this.size = 7;
		this.scale = 3;
		this.cellSpacing = 15;
		this.tileSize = 25;
		this.stuff = null;
		this.stuff = ss.multidimArray(null, this.size, this.size);
	};
	$LD31_World.__typeName = 'LD31.World';
	global.LD31.World = $LD31_World;
	ss.initClass($U, {});
	ss.initEnum($LD31_$Tree$Status, { $sapling: 0, $regular: 1, $apple: 2 });
	ss.initInterface($LD31_IGameObject, { update: null, draw: null });
	ss.initClass($LD31_GameObject, {
		draw: function(g) {
			this.world.drawCell(g, this.get_texture(), this.get_p(), this.get_fliped(), this.get_offset());
		},
		get_p: function() {
			return this.$_pos;
		},
		set_p: function(value) {
			if (!this.world.inBounds(value)) {
				return;
			}
			if (ss.isValue(this.world.get_item(value))) {
				return;
			}
			if ($LD31_Vec2i.op_Inequality(this.$_pos, null)) {
				this.world.set_item(this.$_pos, null);
			}
			this.world.set_item(value, this);
			this.$_pos = value;
		},
		update: function() {
		},
		get_texture: function() {
			return null;
		},
		get_fliped: function() {
			return false;
		},
		get_offset: function() {
			return null;
		}
	}, null, [$LD31_IGameObject]);
	ss.initClass($LD31_Entity, {
		get_texture: function() {
			if (this.animationFramesLeft > 0) {
				return this.texture_jump;
			}
			return this.texture_idle;
		},
		get_offset: function() {
			//Whatever. Going to just assume the are both 15.
			//return null;
			if (this.animationFramesLeft > 0) {
				return $LD31_Vec2i.op_Addition($LD31_Vec2i.op_Multiply(-this.animationFramesLeft, this.$lastMove), $LD31_Vec2i.op_Multiply(ss.Int32.trunc(this.jumpHeight - this.jumpHeight * Math.pow(2 * this.animationFramesLeft / 15 - 1, 2)), $LD31_Vec2i.up));
			}
			return null;
		},
		get_fliped: function() {
			return this.left;
		},
		getMove: function() {
			var move = $LD31_Vec2i.zero;
			if ($LD31_Keyboard.getPressed(119) | $LD31_Keyboard.getDown(38)) {
				move = $LD31_Vec2i.op_Addition(move, $LD31_Vec2i.up);
			}
			if ($LD31_Keyboard.getPressed(115) | $LD31_Keyboard.getDown(40)) {
				move = $LD31_Vec2i.op_Addition(move, $LD31_Vec2i.down);
			}
			if ($LD31_Keyboard.getPressed(97) | $LD31_Keyboard.getDown(37)) {
				move = $LD31_Vec2i.op_Addition(move, $LD31_Vec2i.left);
			}
			if ($LD31_Keyboard.getPressed(100) | $LD31_Keyboard.getDown(39)) {
				move = $LD31_Vec2i.op_Addition(move, $LD31_Vec2i.right);
			}
			return move;
		},
		update: function() {
			if (this.animationFramesLeft > 0) {
				this.animationFramesLeft--;
				return;
			}
			var move = this.getMove();
			if ($LD31_Vec2i.op_Equality(move, $LD31_Vec2i.zero)) {
				return;
			}
			if (move.x > 0) {
				this.left = false;
			}
			if (move.x < 0) {
				this.left = true;
			}
			var newPos = $LD31_Vec2i.op_Addition(this.get_p(), move);
			if (this.world.inBounds(newPos) && ss.isNullOrUndefined(this.world.get_item(newPos))) {
				this.$lastMove = move;
				this.animationFramesLeft = $LD31_Entity.moveNumFrames;
				this.set_p(newPos);
			}
		}
	}, $LD31_GameObject, [$LD31_IGameObject]);
	ss.initClass($LD31_Bunny, {
		get_texture: function() {
			if (this.animationFramesLeft > 0) {
				return $LD31_Bunny.texture_jump;
			}
			return $LD31_Bunny.texture_idle;
		},
		get_fliped: function() {
			return !this.left;
		},
		getMove: function() {
			var move = $LD31_Vec2i.zero;
			if (Math.random() <= 0.01) {
				move = $LD31_Vec2i.op_Addition(move, $LD31_Vec2i.up);
			}
			if (Math.random() <= 0.01) {
				move = $LD31_Vec2i.op_Addition(move, $LD31_Vec2i.down);
			}
			if (Math.random() <= 0.01) {
				move = $LD31_Vec2i.op_Addition(move, $LD31_Vec2i.left);
			}
			if (Math.random() <= 0.01) {
				move = $LD31_Vec2i.op_Addition(move, $LD31_Vec2i.right);
			}
			return move;
		}
	}, $LD31_Entity, [$LD31_IGameObject]);
	ss.initClass($LD31_Game, {
		tick: function() {
			this.world.update();
			$LD31_Keyboard.update();
			this.gl.clear();
			this.world.draw(this.gl);
		},
		$start: function() {
			this.gl = new $LD31_GL(this.world.scale * (this.world.size * this.world.cellSpacing + 10));
			$LD31_Keyboard.start();
			window.setInterval(ss.mkdel(this, this.tick), 16);
			new $LD31_Tree(this.world, new $LD31_Vec2i(3, 3));
			new $LD31_Bunny(this.world, new $LD31_Vec2i(4, 3));
			var $t1 = new $LD31_Entity(this.world, $LD31_Vec2i.zero);
			$t1.texture_idle = new $LD31_Texture2D('res/man_idle.png');
			$t1.texture_jump = new $LD31_Texture2D('res/man_jump.png');
			$t1;
		}
	});
	ss.initClass($LD31_GL, {
		clear: function() {
			this.ctx.fillStyle = 'black';
			this.ctx.fillRect(0, 0, this.$canvas.width, this.$canvas.height);
		},
		drawTexture$1: function(texture, x, y, dw, dh) {
			this.ctx.drawImage(texture.img, x, y, dw, dh);
		},
		drawTexture: function(texture, x, y) {
			this.ctx.drawImage(texture.img, x, y);
		}
	});
	ss.initClass($LD31_ImgLoader, {});
	ss.initEnum($LD31_Key, { left: 37, up: 38, right: 39, down: 40, w: 119, a: 97, s: 115, d: 100 });
	ss.initClass($LD31_Keyboard, {});
	ss.initClass($LD31_Program, {});
	ss.initClass($LD31_Sprite, {});
	ss.initClass($LD31_Texture2D, {});
	ss.initClass($LD31_Tree, {
		get_hasApple: function() {
			return this.age >= $LD31_Tree.timeUntilApple;
		},
		get_texture: function() {
			if (this.get_isSapling()) {
				return $LD31_Tree.treeSaplingTexture;
			}
			if (this.get_hasApple()) {
				return $LD31_Tree.treeWithAppleTexture;
			}
			return $LD31_Tree.treeTexture;
		},
		update: function() {
			this.age++;
		},
		get_isSapling: function() {
			return this.age < 0;
		}
	}, $LD31_GameObject, [$LD31_IGameObject]);
	ss.initClass($LD31_Vec2i, {
		toString: function() {
			return '{' + this.x + ',' + this.y + '}';
		},
		equals: function(o) {
			return this.equalsT(ss.safeCast(o, $LD31_Vec2i));
		},
		equalsT: function(o) {
			if ($LD31_Vec2i.op_Equality(o, null)) {
				return false;
			}
			return this.x === o.x && this.y === o.y;
		},
		getHashCode: function() {
			return ss.getHashCode(this.x) + ss.getHashCode(this.y);
		}
	}, null, [ss.IEquatable]);
	ss.initClass($LD31_World, {
		get_item: function(p) {
			return this.get_item$1(p.y, p.x);
		},
		set_item: function(p, value) {
			this.set_item$1(p.y, p.x, value);
		},
		get_item$1: function(r, c) {
			return ss.arrayGet(this.stuff, r, c);
		},
		set_item$1: function(r, c, value) {
			ss.arraySet(this.stuff, r, c, value);
		},
		inBounds: function(pos) {
			return pos.x >= 0 && pos.y >= 0 && pos.y < this.size && pos.x < this.size;
		},
		drawCell: function(gl, texture, pos, flip, offset) {
			//Console.WriteLine(pos.ToString());
			gl.ctx.save();
			gl.ctx.translate(0, gl.ctx.canvas.height);
			gl.ctx.scale(this.scale, this.scale);
			gl.ctx.translate(this.cellSpacing * pos.x, -texture.img.height - this.cellSpacing * pos.y);
			if ($LD31_Vec2i.op_Inequality(offset, null)) {
				gl.ctx.translate(offset.x, -offset.y);
			}
			if (flip) {
				gl.ctx.scale(-1, 1);
				gl.ctx.translate(ss.Int32.div(texture.img.width, 2) - this.cellSpacing, 0);
			}
			gl.drawTexture(texture, 0, 0);
			gl.ctx.restore();
		},
		draw: function(gl) {
			//Draw grass
			for (var row = 0; row < this.size; row++) {
				for (var col = 0; col < this.size; col++) {
					this.drawCell(gl, $LD31_World.$grass, new $LD31_Vec2i(col, row), false, null);
				}
			}
			for (var y = this.size - 1; y >= 0; y--) {
				for (var x = 0; x < this.size; x++) {
					var t = this.get_item$1(y, x);
					if (ss.isValue(t)) {
						t.draw(gl);
					}
				}
			}
		},
		update: function() {
			var toUpdate = [];
			for (var $t1 = 0; $t1 < this.stuff.length; $t1++) {
				var thing = this.stuff[$t1];
				if (ss.isValue(thing)) {
					ss.add(toUpdate, thing);
				}
			}
			for (var $t2 = 0; $t2 < toUpdate.length; $t2++) {
				var thing1 = toUpdate[$t2];
				thing1.update();
			}
		}
	});
	$LD31_Vec2i.zero = new $LD31_Vec2i(0, 0);
	$LD31_Vec2i.right = new $LD31_Vec2i(1, 0);
	$LD31_Vec2i.up = new $LD31_Vec2i(0, 1);
	$LD31_Vec2i.left = new $LD31_Vec2i(-1, 0);
	$LD31_Vec2i.down = new $LD31_Vec2i(0, -1);
	$LD31_Keyboard.$keysDown = new (ss.makeGenericType(ss.Dictionary$2, [$LD31_Key, Boolean]))();
	$LD31_Keyboard.$justChanged = new (ss.makeGenericType(ss.Dictionary$2, [$LD31_Key, Boolean]))();
	$LD31_Keyboard.$justPressed = [];
	$LD31_Entity.moveNumFrames = 15;
	$LD31_ImgLoader.$cashe = new (ss.makeGenericType(ss.Dictionary$2, [String, Object]))();
	$LD31_Bunny.texture_idle = new $LD31_Texture2D('res/bunny_idle.png');
	$LD31_Bunny.texture_jump = new $LD31_Texture2D('res/bunny_jump.png');
	$LD31_Tree.treeTexture = new $LD31_Texture2D('res/tree.png');
	$LD31_Tree.treeWithAppleTexture = new $LD31_Texture2D('res/tree_with_apple.png');
	$LD31_Tree.treeSaplingTexture = new $LD31_Texture2D('res/tree_sapling.png');
	$LD31_Tree.timeAsSapling = 3600;
	$LD31_Tree.timeUntilApple = 600;
	$LD31_World.$grass = new $LD31_Texture2D('res/ground.png');
	$LD31_Game.ticksPerSecond = 60;
	$LD31_Program.game = null;
})();
