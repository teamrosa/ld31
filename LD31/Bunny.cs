﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LD31
{
    public class Bunny : Entity
    {
        //Can hold
        //
        public Bunny(World world, Vec2i pos) : base(world, pos) { }

        public static Texture2D texture_idle = new Texture2D("res/bunny_idle.png");
        public static Texture2D texture_jump = new Texture2D("res/bunny_jump.png");

        public override Texture2D Texture
        {
            get
            {
                if (AnimationFramesLeft > 0)
                    return texture_jump;
                return texture_idle;
            }
        }

        Vec2i lastMove = Vec2i.Zero;

        public override bool Fliped
        {
            get
            {
                return !left;
            }
        }

        public override Vec2i GetMove()
        {
            var move = Vec2i.Zero;
            if (Math.Random() <= .01)
                move += Vec2i.Up;

            if (Math.Random() <= .01)
                move += Vec2i.Down;

            if (Math.Random() <= .01)
                move += Vec2i.Left;

            if (Math.Random() <= .01)
                move += Vec2i.Right;
            return move;
        }
    }
}