﻿using jQueryApi;
using System;
using System.Collections.Generic;
using System.Html;
using System.Html.Media.Graphics;
using System.Linq;
using System.Web;

namespace LD31
{
    public class Game
    {
        public const int TicksPerSecond = 60;
        public GL gl;
        public World world = new World();

        public void Tick()
        {
            world.Update();
            Keyboard.Update();
            gl.Clear();
            world.Draw(gl);
        }

        internal void Start()
        {
            gl = new GL(world.Scale * (world.Size * world.CellSpacing + 10));
            Keyboard.Start();
            Window.SetInterval(Tick, 1000 / TicksPerSecond);

            new Tree(world, new Vec2i(3, 3));

            new Bunny (world, new Vec2i(4, 3));

            new Entity(world, Vec2i.Zero)
            {
                texture_idle = new Texture2D("res/man_idle.png"),
                texture_jump = new Texture2D("res/man_jump.png")
            };

            
        }
    }
}