﻿using System;
using System.Collections.Generic;
using System.Html;
using System.Html.Media.Graphics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace LD31
{
    public class GL
    {
        readonly CanvasElement canvas;
        public readonly CanvasRenderingContext2D ctx;

        public GL(int size = 345)
        {
            canvas = (CanvasElement)Document.CreateElement("canvas");
            canvas.Height = canvas.Width = size;
            ctx = (CanvasRenderingContext2D)canvas.GetContext("2d");
            SetImageSmoothingOff();
            Document.Body.AppendChild(canvas);
        }

        [InlineCode("{this}.ctx.imageSmoothingEnabled=false")]
        public void SetImageSmoothingOff() { }

        public void Clear()
        {
            ctx.FillStyle = "black";
            ctx.FillRect(0, 0, canvas.Width, canvas.Height);
        }

        public void DrawTexture(Texture2D texture, int x, int y,int dw, int dh)
        {
            ctx.DrawImage(texture.img, x, y,dw,dh);
        }
        public void DrawTexture(Texture2D texture, int x, int y)
        {
            ctx.DrawImage(texture.img, x, y);
        }
    }
}