﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Html;

namespace LD31
{
    public class Texture2D
    {
        public readonly ImageElement img;
        public readonly string name;
        public Texture2D(string name)
        {
            this.name = name;
            img = ImgLoader.Get(name);
        }
    }



    public static class ImgLoader
    {
        static Dictionary<string, ImageElement> cashe = new Dictionary<string, ImageElement>();

        public static ImageElement Get(string path)
        {
            ImageElement img;
            if (cashe.TryGetValue(path, out img))
                return img;
            return cashe[path] = Load(path);
        }

        private static ImageElement Load(string path)
        {
            return new ImageElement()
            {
                Src = path
            };
        }
    }
}