﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LD31
{
    public interface IGameObject
    {
        void Update();
        void Draw(GL gl);
    }

    public class GameObject : IGameObject
    {
        public readonly World world;

        public GameObject(World world, Vec2i pos)
        {
            this.world = world;
            this.P = pos;
        }

        public void Draw(GL g)
        {
            world.DrawCell(g, Texture, P, Fliped, Offset);
        }

        private Vec2i _pos;

        public Vec2i P
        {
            get
            {
                return _pos;
            }
            set
            {
                if (!world.InBounds(value))
                    return;
                if (world[value] != null)
                    return;
                if (_pos != null)
                    world[_pos] = null;
                world[value] = this;
                _pos = value;
            }
        }

        public virtual void Update() { }
        public virtual Texture2D Texture { get { return null; } }
        public virtual bool Fliped { get { return false; } }
        public virtual Vec2i Offset { get { return null; } }
    }
}