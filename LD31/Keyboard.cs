﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Html;

namespace LD31
{
    public enum Key
    {
        Left = 37, Up = 38, Right = 39, Down = 40, W=119, A=97, S=115, D=100
    }

    public static class Keyboard
    {
        static readonly Dictionary<Key, bool> keysDown = new Dictionary<Key, bool>();
        static readonly Dictionary<Key, bool> justChanged = new Dictionary<Key, bool>();

        static readonly List<Key> justPressed = new List<Key>();

        public static void Update()
        {
            justPressed.Clear();
            justChanged.Clear();
        }


        public static bool GetKey(Key key)
        {
            return keysDown.Get(key, false);
        }

        public static bool GetPressed(Key key)
        {
            return justPressed.Contains(key);
        }

        public static bool GetDown(Key key)
        {
            return GetKey(key) && justChanged.Get(key, false);
        }


        public static void Start()
        {
            Document.OnKeypress = (e) =>
            {
                var k = (KeyboardEvent)e;
                
                var key = (Key)k.KeyCode;
                //Console.WriteLine("Key: "+key);
                justPressed.Add(key);
            };

            Document.OnKeydown = (e) =>
            {
                var k = (KeyboardEvent)e;
                var key = (Key)k.KeyCode;
                justChanged[key] = !GetKey(key);
                keysDown[key] = true;
            };

            Document.OnKeyup = (e) =>
            {
                var k = (KeyboardEvent)e;
                var key = (Key)k.KeyCode;
                justChanged[key] = GetKey(key);
                keysDown[key] = false;
            };
        }


    }
}